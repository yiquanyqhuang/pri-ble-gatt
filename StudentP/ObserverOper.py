from ObserverClass import Register, UnRegister

from BLEmodule import AnyDeviceManager, AnyDevice



class Serilcenter(UnRegister):
    """熱水器：戰勝寒冬的有力武器"""
    def __init__(self):
        super().__init__()
        self.__getdata = ''
        self.__functype = b''
    def getgetdata(self):
        return self.__getdata

    def setgetdata(self, datafromcom):
        self.__getdata = datafromcom
        strdata = self.__getdata.strip(b'\r\n').decode('ascii')
        print( "get byte datas：" + strdata )
        self.notifyObservers()

''' 
    c:99   
    w:119
    r:114
    p:112  

'''

class ChangeProject(Register):
    """該模式用於轉換專案"""
    def update(self, Register, object):
        self.__functype = Register.getgetdata()[3]


        arrPCcmd = Register.getgetdata().strip(b'\r\n').decode('ascii').split(' ', 3);

        if isinstance(Register, Serilcenter)  and self.__functype == 112:
            if(arrPCcmd[2] == 'cxr6'):
                projects = uuids.cxr6()
                print("更改專案為cxr6")
            elif(arrPCcmd[2] == 'lite'):
                projects = uuids.cxrlift()
                print("更改專案為lite")


class SendtoChara(Register):
    "傳該模式用於傳輸"
    def update(self, Register, object):
        self.__functype = Register.getgetdata()[3]
        if isinstance(Register, Serilcenter) and self.__functype == 119:
            print("傳送數據")


class ReadfromChara(Register):
    "傳該模式用於傳輸"
    def update(self, Register, object):
        self.__functype = Register.getgetdata()[3]
        if isinstance(Register, Serilcenter) and self.__functype == 114:
            print("讀取數據")

class Connecttodevice(Register):
    "傳該模式用於傳輸"
    def update(self, Register, object):
        self.__functype = Register.getgetdata()[3]
        if isinstance(Register, Serilcenter) and self.__functype == 99:
            print("與待測物連線")

            arrPCcmd = Register.getgetdata().strip(b'\r\n').decode('ascii').split(' ', 3);
            print (len(arrPCcmd))
            if(len(arrPCcmd) >= 3):
                manager = AnyDeviceManager(adapter_name='hci0')
                MACaddress = arrPCcmd[3]
                device = AnyDevice(mac_address=MACaddress, manager=manager)

                device.connect()
            # time.sleep(3)